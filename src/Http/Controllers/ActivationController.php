<?php

namespace EeeInnovation\ProjectActivator\Http\Controllers;

use App\Http\Controllers\Controller;
use EeeInnovation\ProjectActivator\Models\LicenseKey;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;


class ActivationController extends Controller
{
    public function index()
    {
        return view('project-activator::activator');
    }

    public function verifyLicense(Request $request)
    {
        try {
            $response = (new Client())->request('POST', 'http://activator.eeeinnovation.com/api/keys/update-license',
                [
                    'query' =>  [
                        'project_slug'=>config('app.project_slug'),
                        'unique_key'=>$request->unique_key,
                        'license_key'=>$request->license_key,
                        'machine_id'=>$this->UniqueMachineID()
                    ],
                    'headers'=>[
                        'Accept'     => 'application/json',
                    ]
                ]
            );
            if ($response->getStatusCode()==200)
            {
                $license_key=LicenseKey::first();
                if ($license_key)
                    $license_key->update(['key'=>$request->license_key]);
                else
                    LicenseKey::create(['key'=>$request->license_key]);
                return redirect('/')->with('message','Activation Successful');
            }
        } catch (ClientException $e) {
            if($e->getResponse()->getStatusCode()==404||$e->getResponse()->getStatusCode()==422)
            return view('project-activator::activator',['message'=>json_decode((string)$e->getResponse()->getBody(true))->message]);

            return view('project-activator::activator',['message'=>'Something went wrong. Please try again or reload the browser']);
        }
    }
    private function UniqueMachineID($salt = "") {
        $result='';
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $result = shell_exec("wmic diskdrive get serialnumber");
        } else {
            $result = shell_exec("lsblk --nodeps -no serial /dev/sda");
        }
        return md5($salt.md5($result));
    }
}
