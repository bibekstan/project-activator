<?php

namespace EeeInnovation\ProjectActivator\Http\Middleware;

use Carbon\Carbon;
use Closure;
use EeeInnovation\ProjectActivator\Models\LicenseKey;
use GuzzleHttp\Client;
use GuzzleHttp\Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;

class CheckLicenseExpiry
{
    /**
     * Handle an incoming request.
     *
     * @param   Request $request
     * @param \Closure $next
     * @return mixed
     * @throws Exception\GuzzleException
     */
    public function handle($request, Closure $next)
    {
        $key = LicenseKey::first();
        if ($key) {
            try {
                $response = (new Client())->request('POST', 'http://activator.eeeinnovation.com/api/keys/verify-license',
                    [
                        'query' => [
                            'project_slug'=>config('app.project_slug'),
                            'license_key' => $key->key,
                            'machine_id'=>$this->UniqueMachineID()
                        ],
                        'headers'=>[
                            'Accept'     => 'application/json',
                        ]
                    ]
                );
                if ($response->getStatusCode() == 200) {
                    $arr = json_decode((string)$response->getBody(), true);
                    $latest_key = $key;
                     if (isset($arr)&&array_key_exists('key', $arr)&&$key->key!=$arr['key']) {
                        $latest_key = tap(LicenseKey::first())->update(['key' => $arr['key']]);
                    }
                    if ($this->checkExpiry($latest_key))
                        return $next($request);
                }
            } catch (ConnectException $exception) {
                $latest_key = LicenseKey::first();
                if ($this->checkExpiry($latest_key))
                    return $next($request);
                else
                    return redirect('activator');
            }
            catch (ClientException $e) {
                return redirect('activator');
            }
        }
        return redirect('activator');
    }

    public function checkExpiry($latest_key)
    {
        try {
            $expiry = explode('.', $latest_key->key);
            if (isset($expiry) && array_key_exists(1, $expiry)) {
                $expiry = base64_decode($expiry[1]);
                $day_remaining = date_diff(Carbon::today(), Carbon::parse($expiry))->format("%r%a");
                if ($day_remaining >= 0) {
                    return true;
                }
            }
        }
        catch (\Exception $exception){
            return false;
        }
        return false;
    }
   private function UniqueMachineID($salt = "") {
        $result='';
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $result = shell_exec("wmic diskdrive get serialnumber");
        } else {
            $result = shell_exec("lsblk --nodeps -no serial /dev/sda");
        }
        return md5($salt.md5($result));
    }
}
