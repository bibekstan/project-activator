<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .content {
            text-align: center;
            width: 100%;
        }

        .title {
            font-size: 65px;
            margin-bottom: 90px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        input[type=text] {
            padding: 10px;
            margin: 10px 0;
            border: 2px solid #eee;
            box-shadow:0 0 15px 4px rgba(0,0,0,0.06);
            outline-color: #3F51B5;
            width: 30%;
        }
        .form-control{
            display: flex;
            align-items: baseline;
            justify-content: center;
        }
        .form-control .label{
            width: 15%;
        }
        button{
            padding:10px;
            border:none;
            background-color:#3F51B5;
            color:#fff;
            font-weight:600;
            border-radius:5px;
            width:30%;
            margin: 10px 13px;
            cursor: pointer;
            outline: none;
        }
        button:hover{
            background: #6C7BB5;
        }
        .alert {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
            min-width: 25%;
            border: 1px solid transparent;
            border-radius: .25rem;
            padding: .75rem 1.25rem;
            margin-top: 15px;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }
    </style>
</head>
<body>
    <div class="content">
        <div class="title">
            Activate your Software
        </div>
            <form action="{{route('activator')}}" method="post">
                @csrf
                <div class="form-control">
                    <div class="label">
                        <label for="slug">Unique Key</label>
                    </div>
                    <input autocomplete="off" value="{{old('slug')}}" name="unique_key" id="slug" type="text" required>
                </div>
                <div class="form-control">
                    <div class="label">
                        <label for="license_key">License Key</label>
                    </div>
                    <input autocomplete="off" value="{{old('slug')}}" name="license_key" id="license_key" type="text" required>
                </div>
                <div class="form-control">
                    <div class="label">
                    </div>
                    <button>Submit</button>
                </div>
            </form>
        @isset($message)
            <div class="form-control">
                <div class="label">
                </div>
                <div class="alert">
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                    <strong>Error! </strong> {{ $message }}
                </div>
            </div>
            @endisset

    </div>
</body>
</html>
