<?php

namespace EeeInnovation\ProjectActivator\Models;

use Illuminate\Database\Eloquent\Model;

class LicenseKey extends Model
{
    protected $fillable=['key'];
}
