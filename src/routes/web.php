<?php

Route::group(['namespace'=>'EeeInnovation\ProjectActivator\Http\Controllers'],function (){
    Route::get('activator','ActivationController@index')->name('activator');
    Route::post('activator','ActivationController@verifyLicense');
});