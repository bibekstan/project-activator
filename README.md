Package to activate project through license key and monitor whether the key is expired or not.

Setup
-composer require eeeinnovation/project-activator
-php artisan migrate
-register middleware in App/Http/Kernel.php like:
    'isLicenseActive' => \EeeInnovation\ProjectActivator\Http\Middleware\CheckLicenseExpiry::class,
-guard web route with the registered middleware like:
    Route::get('/', function () {
        return view('welcome');
    })->middleware('isLicenseActive');
-Declare a config variable 'project_slug' in app.php like:
    'project_slug'=> PROJECT SLUG